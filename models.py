
from pony.orm import Database, Required, Optional, Set, PrimaryKey

db = Database()


class Professor(db.Entity):
    id = PrimaryKey(int, auto=True)
    name = Required(str)
    password = Required(str)
    subject = Required("Subject", reverse="professor")
    students = Set("Student", reverse="professors")

class Subject(db.Entity):
    id = PrimaryKey(int, auto=True)
    name = Required(str)
    students = Set("Student", reverse="subjects")
    professor = Optional("Professor", reverse="subject")

class Student(db.Entity):
    id = PrimaryKey(int, auto=True)
    name = Required(str)
    password = Required(str)
    subjects = Set("Subject", reverse="students")
    professors = Set("Professor", reverse="students")
    parent = Optional("Parent", reverse="students")

class Parent(db.Entity):
    id = PrimaryKey(int, auto=True)
    name = Required(str)
    password = Required(str)
    #students = Required(Student)
    students = Optional("Student", reverse="parent")

db.bind(provider='sqlite', filename='smsdb', create_db=True)
db.generate_mapping(create_tables=True)

