from flask import Flask, render_template, redirect, url_for, request
from pony.orm import select, db_session
from models import Professor, Student, Subject, Parent


app = Flask(__name__)


@app.route('/')
def index():

    return render_template('index.html')

@app.route('/signup_student', methods=['GET', 'POST'])
@db_session
def signup_student():

    if request.method == 'GET':
        return render_template('signup_student.html')

    else:
        data = request.form
        if data['password'] == data['confirmpassword']:
            Student(name=f'{data["name"]} {data["surname"]}', password=data['password'])

            return redirect(url_for('.student_page'))

        else:
            return render_template('signup_student.html')


@app.route('/login_student')
def login_student():
    return render_template('login_student.html')

@app.route('/signup_teacher')
@db_session
def signup_teacher():
    if request.method == 'GET':
        return render_template('signup_teacher.html')

    else:
        data = request.form
        if data['password'] == data['confirmpassword']:
            Professor(name=f'{data["name"]} {data["surname"]}', password=data['password'])

            return redirect(url_for('.teacher_page'))

        else:
            return render_template('signup_teacher.html')


@app.route('/login_teacher')
def login_teacher():
    return render_template('login_teacher.html')

@app.route('/signup_parent')
def signup_parent():

    if request.method == 'GET':
        return render_template('signup_parent.html')

    else:
        data = request.form
        if data['password'] == data['confirmpassword']:
            Parent(name=f'{data["name"]} {data["surname"]}', password=data['password'])

            return redirect(url_for('.parent_page'))

        else:
            return render_template('signup_parent.html')


@app.route('/login_parent')
def login_parent():
    return render_template('login_parent.html')


@app.route('/student_page')
def student_page():
    return render_template('student_page.html')

@app.route('/teacher_page')
def teacher_page():
    return render_template('teacher_page.html')


@app.route('/parent_page')
def parent_page():
    return render_template('parent_page.html')

@app.route('/review')
@db_session
def review():

    students = select(s for s in Student)
    profs = select(p for p in Professor)
    subject = select(s for s in Subject)

    return render_template('review.html', STUDENTS=students, PROFS=profs)









